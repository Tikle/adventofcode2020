import os

def fuelNeeded(mass):
    return mass // 3 - 2

def recursiveFuelNeeded(mass):
    fuel = fuelNeeded(mass)
    if fuel <= 0:
        return 0 
    else: 
        return fuel + recursiveFuelNeeded(fuel) 

with open(os.path.join("1","input.txt"), "r") as inputFile:
    moduleList = (int(line) for line in inputFile.read().splitlines())
    totalFuelNeeded = sum(recursiveFuelNeeded(module) for module in moduleList)

print(totalFuelNeeded)