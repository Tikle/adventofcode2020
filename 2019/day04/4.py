inputRange = [134792, 675810]
counter = 0

def hasDouble(number):
    numStr = str(number)
    for digit in range(len(numStr) - 1):
        if numStr[digit] == numStr[digit + 1]:
            return True
    return False

def hasExactlyADouble(number):
    numStr = str(number)
    combo = 1
    smallestCombo = len(numStr) + 1
    for digit in range(len(numStr) - 1):
        if numStr[digit] != numStr[digit + 1]:
            if combo < smallestCombo and combo != 1:
                smallestCombo = combo
                combo = 1  
        else:
            combo += 1
            if digit + 1 == len(numStr) - 1:
                if combo < smallestCombo and combo != 1:
                    smallestCombo = combo
                    combo = 1 
    if smallestCombo == 2:
        return True
    else:
        return False

def isIncreasing(number):
    numStr = str(number)
    for digit in range(len(numStr) - 1):
        if numStr[digit] > numStr[digit + 1]:
            return False
    return True
  

for i in range(inputRange[0], inputRange[1] + 1):
    if hasExactlyADouble(i) and isIncreasing(i):
        counter += 1
        
print(counter)

