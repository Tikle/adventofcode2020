from re import findall

with open('day14.input', 'r') as input_file:
    init_program = input_file.read().splitlines()

mem = {}

# Part 1
for line in init_program:
    if line[1] == 'a': #lazy test
        mask = findall("^mask = ([01X]{36})$", line)[0]
        mask0 = int(mask.replace('X', '1'), base=2) 
        mask1 = int(mask.replace('X', '0'), base=2)
    else:
        address, value = findall("^mem\[(\d+)\] = (\d+)$", line)[0]
        result = (int(value) & mask0) | mask1
        mem[int(address)] = result    

print(sum(mem.values()))

# Part 2
my_sum=0
sup = len(init_program) - 1
inf = sup
answer = 0
flagged = set()

def decline(address, maskx):
    address_list = set()
    posX = []
    for i in range(len(maskx)):
        if maskx[i] == 'X':
            posX.append(i)

    numX = len(posX)
    for binary in range(1, 2 ** numX + 1):
        new_address = str(bin(address)[2:].zfill(len(maskx)))
        binarystr = list(str(bin(binary)[2:].zfill(numX)))
        for pos in posX:
            tmp=list(new_address)
            tmp[pos] = binarystr.pop()
            new_address = "".join(tmp)
        address_list.add(new_address)
    return address_list


while inf > 0:
    while init_program[inf][1] != 'a':
        inf -= 1
    mask = findall("^mask = ([01X]{36})$", init_program[inf])[0]

    for line in range(sup, inf, -1):
        address, value = findall("^mem\[(\d+)\] = (\d+)$", init_program[line])[0]
        mask1 = int(mask.replace('X', '0'), base=2)
        maskx = mask.replace('1', '0')
        address = int(address) | mask1
        for sub_address in decline(address, maskx):
            if sub_address not in flagged:
                flagged.add(sub_address)
                answer += int(value)
    inf -= 1
    sup = inf
    
print(answer)
