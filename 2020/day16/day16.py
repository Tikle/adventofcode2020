from re import findall

with open('day16.input', 'r') as input_file:
    data = input_file.read().splitlines()

fields = [findall("(.+):", line) for line in data[0:20]]
limits = [list(map(int, findall("(\d{1,3})", line))) for line in data[0:20]]
burn = list(map(int, data[22].split(',')))
tickets = [list(map(int, line.split(','))) for line in data[25:]]

def into_limits(number, limit):
    return number in range(limit[0], limit[1] + 1) or number in range(limit[2], limit[3] + 1)

# Part 1
result = 0
good_tickets = tickets.copy()
for ticket in tickets:
    for number in ticket:
        number_valid = False
        
        for limit in limits:
            if into_limits(number, limit):
                number_valid = True
                break

        if not number_valid:
            result += number
            good_tickets.remove(ticket)
            break

print(result)

# Part 2
system = dict()
for i, limit in enumerate(limits):
    possibilities = set()
    for field_tested in range(len(fields)):
        valid=True
        for good_ticket in range(len(good_tickets)):
            value = good_tickets[good_ticket][field_tested]
            if not into_limits(value, limit):
                valid=False
                break
        if valid:
            possibilities.add(field_tested)
            valid=False
    system[i] = possibilities

matches = dict()
known = set()
for greedy in sorted(system, key=lambda k: len(system[k])):
    matches[greedy] = system[greedy].difference(known)
    known = known.union(matches[greedy])

cloud=1
for i in range(6):
    fire = matches[i].pop()
    smoke = burn[fire]
    cloud *= smoke
print(cloud)

    



