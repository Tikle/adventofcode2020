with open('day8.input', 'r') as input_file:
    program = [instruction.split() for instruction in input_file.read().splitlines()]

def run(p, swap_line = None):
    line, acc, flag = 0, 0, set()
    while line < len(p):
        op, arg = p[line][0], int(p[line][1])
        if line in flag:
            if swap_line == None: print("Loop detected with acc =", acc)
            return(1)
        else:
            flag.add(line)
            if line == swap_line:
                if op == 'jmp': op = 'nop'
                elif op == 'nop': op = 'jmp'
            if op == 'acc':
                acc += arg
                line += 1
            elif op == 'jmp':
                line += arg
            else:
                line += 1
    print("Program ended normally with acc =", acc, "by swapping instruction at line", swap_line)

# Part 1
run(program)

# Part 2
for lineswap in range(len(program)):
    run(program, lineswap)
