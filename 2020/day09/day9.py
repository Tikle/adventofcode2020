from itertools import combinations

with open('day9.input', 'r') as input_file:
    xmas = [int(line) for line in input_file.read().splitlines()]

# Part 1
def cypher_breach():
    for k in range(25, len(xmas)):
        preamble=xmas[k-25:k]
        number=xmas[k]
        possibilities=[x+y for x,y in list(combinations(preamble, 2))]
        if number not in possibilities:
            print("The breach is the number",number,"at position",k)
            return(number)

# Part 2
def crack_cypher(n):
    for k in range(0, len(xmas)):
        acc, i = xmas[k], 0
        while acc < n:
            i += 1
            acc += xmas[k+i]
            if acc == n:
                print("The weakness is", min(xmas[k:k+i])+max(xmas[k:k+i]), "with the sequence between",xmas[k], "at position", k, "and", xmas[k+i], "at position", k+i, "which add up to",n)
                return(0)
            
crack_cypher(cypher_breach())
        