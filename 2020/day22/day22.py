with open('day22.input', 'r') as input_file:
    data = input_file.read().splitlines()
    mydeck = list(map(int, data[1:26]))
    crabsdeck = list(map(int, data[28:53]))

def score(d1, d2):
    multiplicator = [x for x in range(len(d1) + len(d2), 0, -1)]
    print("Player 1 score :", sum(list(multiplicator[k] * d1[k] for k in range(len(d1)))))
    print("Player 2 score :", sum(list(multiplicator[k] * d2[k] for k in range(len(d2)))))

# Part 1
def combat(d1, d2):
    deck1 = d1.copy()
    deck2 = d2.copy()
    while len(deck1) > 0 and len(deck2) > 0:
        card1 = deck1.pop(0)
        card2 = deck2.pop(0)
        if card1 > card2:
            deck1.append(card1)
            deck1.append(card2)
        if card2 > card1:
            deck2.append(card2)
            deck2.append(card1)
    score(deck1, deck2)

# Part 2
def recursive_combat(d1, d2, level = 0):
    deck1 = d1.copy()
    deck2 = d2.copy()
    previous1 = list()
    previous2 = list()
    while len(deck1) > 0 and len(deck2) > 0:
        previous1.append(deck1.copy())
        previous2.append(deck2.copy())
        winner = 0
        if deck1[0] < len(deck1) and deck2[0] < len(deck2):
            winner = recursive_combat(deck1[1:deck1[0]+1], deck2[1:deck2[0]+1], 1)
        card1 = deck1.pop(0)
        card2 = deck2.pop(0)
        if (winner == 0 and card1 > card2) or winner == 1:
            deck1.append(card1)
            deck1.append(card2)
        elif (winner == 0 and card2 > card1) or winner == 2:
            deck2.append(card2)
            deck2.append(card1)
        if deck1 in previous1 and deck2 in previous2:
            return 1
        
    if level == 0: score(deck1, deck2)
    if len(deck1) == 0: return 2
    elif len(deck2) == 0: return 1
    else: return 0

combat(mydeck, crabsdeck)
recursive_combat(mydeck, crabsdeck)