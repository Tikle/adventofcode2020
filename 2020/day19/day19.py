from re import match

with open('day19.input', 'r') as input_file:
    data = input_file.read().splitlines() 
    rules = dict()
    for rule in data[:129]:
        values = rule.split(': ')
        rules[values[0]] = values[1]
    messages = data[130:]

def regex(rule_id):
    values = rules[rule_id]
    if values[1].isalpha():
        return values[1]
    final_regex = ''
    # Part 2 begins
    if rule_id == '8':
        final_regex += regex('42') + '+'
    elif rule_id == '11':
        c = regex('42')
        d = regex('31')
        final_regex += '(?:' + c + d + '|' + c * 2 + d * 2 + '|' + c * 3 + d * 3 + '|' + c * 4 + d * 4 + ')'
    else:
    # Part 2 ends
        final_regex += '(?:'
        for possibility in values.split('|'):
            for subrule_id in possibility.split():
                final_regex += regex(subrule_id)
            final_regex += '|'
        final_regex = final_regex[:-1]
        final_regex += ')'
    return(final_regex)

print(len(list(message for message in messages if match('^' + regex('0') + '$', message))))