from math import gcd

with open('day13.input', 'r') as input_file:
    input = input_file.readlines()

# Part 1
my_ts = int(input[0])
bus_ids = [int(id) for id in input[1].split(',') if id != 'x']
next_departures = {id : id * (my_ts // id + 1) for id in bus_ids}

print("ID of the earliest bus x time left =", \
    [key * (value - my_ts) for key, value in next_departures.items() if value == min(next_departures.values())])

# Part 2
next_departures = {i: int(dep) for i, dep in enumerate(input[1].split(',')) if dep != 'x'}

time = 0
increment = 1
for delay, bus_id in next_departures.items():
    while (time + delay) % bus_id != 0:
        time += increment # Check only times that match previously checked IDs
    increment *= bus_id # Bus IDs are prime numbers, lcm not needed

print('The time when each bus departs in order every minute is', time)

# Bonus
def lcm(l):
    if len(l) == 1:
        return l[0]
    else:
        next = l.pop()
        p = lcm(l)
        return next * p // gcd(next, p)

