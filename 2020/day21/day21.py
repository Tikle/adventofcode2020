from re import findall

with open('day21.input', 'r') as input_file:
    data = findall("((?:\w+ )+)\(contains ((?:\w(?:, )?)+)\)", input_file.read())
    ingredients = tuple(set(line[0].split(' ')[:-1]) for line in data)
    allergens = tuple(set(line[1].split(', ')) for line in data)

all_allergens = set.union(*allergens)
all_ingredients = set.union(*ingredients)

allergen_dict = dict()
known_allergens = set()
while len(known_allergens) != len(all_allergens):
    for allergen in all_allergens.difference(known_allergens):
        reduced = set.intersection(*[ingredients[i] for i in range(len(ingredients)) if allergen in allergens[i]]).difference(known_allergens)
        if len(reduced) == 1:
            new = reduced.pop()
            allergen_dict[allergen] = new
            known_allergens.add(new)

good_ingredients = all_ingredients.difference(set(allergen_dict.values()))

print("The list contains", sum(list(list(ingredient).count(ing) for ingredient in ingredients for ing in good_ingredients)), "good ingredients.")

print("The canonical dangerous ingredient list is :",','.join(allergen_dict[k] for k in sorted(allergen_dict)))