from re import findall

# Reading password database into an array
with open('day2.input', 'r') as input_file:
    password_database = findall("(\d+)-(\d+) (\w): (\w+)\s", input_file.read())

# Counting valid passwords
valid_passwords = [password for nb_min, nb_max, character, password in password_database 
    if int(nb_min) <= password.count(character) <= int(nb_max)]
print("There are", len(valid_passwords), "valid passwords according to the shopkeeper.")

# Counting truly valid passwords
OTCAS_valid_passwords = [password for position_1, position_2, character, password in password_database 
    if (password[int(position_1) - 1] == character) ^ (password[int(position_2) - 1] == character) ]
print("There are", len(OTCAS_valid_passwords), "valid passwords on the Official Toboggan Corporate Authentification System.")