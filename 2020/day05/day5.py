with open('day5.input', 'r') as input_file:
    boarding_passes = input_file.read().splitlines()

conversion_table = {
    'F': '0',
    'B': '1',
    'L': '0',
    'R': '1'
}

def seat_id(bpass):
    for letter, number in conversion_table.items():
        bpass = bpass.replace(letter, number)
    row = int(bpass[0:7], 2)
    column = int(bpass[7:10], 2)
    return(row * 8 + column)

def find_missing_id(id_table):
    id_table.sort()
    for i in range(len(boarding_ids)):
        if id_table[0] + i != id_table[i]:
            return(id_table[i] - 1)

boarding_ids = [seat_id(bpass) for bpass in boarding_passes]

print("The maximum ID is", max(boarding_ids))

print("My ID is", find_missing_id(boarding_ids))