with open('day10.input', 'r') as program:
    values = [line.split()[-1] for line in program.read().splitlines()]

# Part 2
def draw(cycle, register):
    drawing_position = (cycle - 1) % 40
    if drawing_position in range(register - 1, register + 2):
        print('#', end="")
    else:
        print('.', end="")
    if drawing_position == 39:
        print()

register = 1
cycle = 1
signal_strengths=[]
for value in values:
    signal_strengths.append(cycle * register)
    draw(cycle, register)
    cycle += 1
    if value != 'noop':
        signal_strengths.append(cycle * register)
        draw(cycle, register)
        cycle += 1
        register += int(value)

# Part 1
print(sum([signal_strengths[cycle] for cycle in range(19, 220, 40)]))