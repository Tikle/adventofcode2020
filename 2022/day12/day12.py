with open('day12.input', 'r') as heightmap_data:
    heightmap = heightmap_data.read().strip().splitlines()

def next_squares(square):
    x, y = square
    neighbor_list = []
    for dx in [-1, 1]:
        if 0 <= x + dx < len(heightmap) and ord(heightmap[x][y]) - ord(heightmap[x + dx][y]) >= - 1:
            neighbor_list.append((x + dx, y))
    for dy in [-1, 1]:
        if 0 <= y + dy < len(heightmap[0]) and ord(heightmap[x][y]) - ord(heightmap[x][y + dy]) >= - 1:
            neighbor_list.append((x, y + dy))
    return neighbor_list

# Finding starting, ending and lowest squares
low_squares = []
for x, y in [(x, y) for x in range(len(heightmap)) for y in range(len(heightmap[0]))]:
    if heightmap[x][y] == 'S':
        start = (x, y)
        heightmap[x] = heightmap[x].replace('S', 'a')
    if heightmap[x][y] == 'E':
        end = (x, y)
        heightmap[x] = heightmap[x].replace('E', 'z')
    if heightmap[x][y] == 'a':
        low_squares.append((x, y))

def shortest_path_length(starting_square):
    visited_squares = set()
    queue = [[starting_square]]
    while queue:
        path_to_square = queue.pop(0)
        square = path_to_square[-1]
        if square == end:
            return len(path_to_square) - 1
        for next_square in next_squares(square):
            if next_square not in visited_squares:
                visited_squares.add(next_square)
                queue.append(path_to_square + [next_square])
    else:
        return 1000 # Sometimes no path exists
                
# Part 1
print(shortest_path_length(start))

# Part 2
print(min([shortest_path_length(square) for square in low_squares]))
