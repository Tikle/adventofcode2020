from itertools import combinations

with open('day18.input', 'r') as scan:
    lava_cubes = [tuple(int(number) for number in line.split(',')) for line in scan.read().splitlines()]

MIN = -2
MAX = 23

def neighbors(cube):
    x, y, z = cube
    candidates = [
        (x + 1, y, z), (x - 1, y, z),
        (x, y + 1, z), (x, y - 1, z),
        (x, y, z + 1), (x, y, z - 1)
    ]
    return [(x, y, z) for x, y, z in candidates if MIN < x < MAX and MIN < y < MAX and MIN < z < MAX]

def is_neighbor(cube, other_cube):
    return other_cube in neighbors(cube)

def external_space():
    queue = [[(0, 0, 0)]]
    visited_cubes = set()
    while queue:
        path = queue.pop(0)
        cube = path[-1]
        for next_cube in neighbors(cube):
            if next_cube not in visited_cubes and next_cube not in lava_cubes:
                visited_cubes.add(next_cube)
                next_path = path + [next_cube]
                queue.append(next_path)
    return visited_cubes

# Part 1
print(6 * len(lava_cubes) - 2 * sum([is_neighbor(cube1, cube2) for cube1, cube2 in combinations(lava_cubes, 2)]))

# Part 2
water_cubes = external_space()
print(sum([is_neighbor(water_cube, lava_cube) for water_cube in water_cubes for lava_cube in lava_cubes]))
