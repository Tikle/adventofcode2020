class Monkey():
    def __init__(self, notes):
        self.number = notes[0].split(':')[0].split()[-1]
        self.starting_items = list(map(int, notes[1].split(': ')[1].split(', ')))
        self.operation = lambda old: eval(notes[2].split('=')[1])
        self.divisor = int(notes[3].split()[-1])
        self.true_monkey = int(notes[4].split()[-1])
        self.false_monkey = int(notes[5].split()[-1])
        self.activity = 0
              
    def __repr__(self):
        return f'Monkey {self.number}:\n  Starting items: {self.starting_items}\n  Operation: new = {self.operation}\n  Test: divisible by {self.divisor}\n    If true: throw to monkey {self.true_monkey}\n    If false: throw to monkey {self.false_monkey}\n'

    def inspect(self, item, extra_worry=False):
        item = self.operation(item)
        self.activity += 1
        if extra_worry:
            return item % common_multiple
        else:
            return item // 3
    
    def throw(self, item):
        if item % self.divisor == 0:
            monkey = monkeys[self.true_monkey]
        else:
            monkey = monkeys[self.false_monkey]
        monkey.starting_items.append(item)
    
    def play_turn(self, extra_worry=False):
        for item in self.starting_items:
            item = self.inspect(item, extra_worry)
            self.throw(item)
        self.starting_items = []
        
with open('day11.input', 'r') as input_file:
    monkeys = [Monkey(monkey_note.split('\n')) for monkey_note in input_file.read().split('\n\n') ]

def monkey_business(rounds, extra_worry=False):
    for _ in range(rounds):
        for monkey in monkeys:
            monkey.play_turn(extra_worry)
    activities=sorted([monkey.activity for monkey in monkeys], reverse=True)
    return activities[0] * activities[1]

# Part 1
print(monkey_business(20))

# Part 2
from math import gcd
from functools import reduce

def lcm(numbers):
    return reduce(lambda a, b: a * b // gcd(a, b), numbers)

common_multiple = lcm([monkey.divisor for monkey in monkeys])

print(monkey_business(10000, extra_worry=True))
