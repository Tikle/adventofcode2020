with open('day20.input', 'r') as encrypted_file:
    encrypted_file = [int(line) for line in encrypted_file.read().splitlines()]

def grove_coordinates(file):
    zero_index = [index for index, number in enumerate(file) if number == 0][0]
    return tuple(file[index] for index in ((zero_index + k) % len(file) for k in [1000, 2000, 3000]))
    
def mix(file, rounds=1):
    size = len(file)
    mixed_indices = list(range(size))
    for round in range(rounds):
        for number_index, steps in enumerate(file):
            index_position = [position for position, mixed_index in enumerate(mixed_indices) if mixed_index == number_index][0]
            moving_index = mixed_indices.pop(index_position)      
            new_position = (index_position + steps) % (size - 1) # the moving index is not in the list  
            mixed_indices.insert(new_position, moving_index)
    return [file[index] for index in mixed_indices]

def apply_key(file, key):
    return [number * key for number in file]

# Part 1
print(sum(grove_coordinates(mix(encrypted_file))))

# Part 2
decrypted_file = apply_key(encrypted_file, 811589153)
print(sum(grove_coordinates(mix(decrypted_file, rounds=10))))
