with open('day17.input', 'r') as scan:
    jet_pattern = scan.read()
    
rocks = [
    [(0, 0), (1, 0), (2, 0), (3, 0)],           # horizontal
    [(1, 0), (0, 1), (1, 1), (2, 1), (1, 2)],   # cross
    [(0, 0), (1, 0), (2, 0), (2, 1), (2, 2)],   # corner
    [(0, 0), (0, 1), (0, 2), (0, 3)],           # vertical
    [(0, 0), (1, 0), (0, 1), (1, 1)]            # square
]  

jet_index = 0
rock_index = 0
CHAMBER_WIDTH = 7
SPAWN_X = 2
SPAWN_Y = 4
solid_rocks = set()
gravity = (0, -1)

def next_jet():
    global jet_index
    direction = jet_pattern[jet_index]
    jet_index = (jet_index + 1) % len(jet_pattern)
    if direction == '<':
        return (-1, 0)
    if direction == '>':
        return (1, 0)
    raise Exception('Bad jet input')

def next_rock(initial_position):
    global rock_index
    shape = rocks[rock_index]
    rock_index = (rock_index + 1) % len(rocks)
    dx, dy = initial_position
    return [(x + dx, y + dy) for x, y in shape]

def moved_rock(rock, movement):
    dx, dy = movement
    return [(x + dx, y + dy) for x, y in rock]

def is_position_legal(rock):
    for x, y in rock:
        if not (0 <= x < CHAMBER_WIDTH): # Into a wall
            return False
        if not (0 < y): # Into the floor
            return False
        if (x, y) in solid_rocks: # Into another rock
            return False
    return True
        
def fall_rocks(rock_quantity, search_cycle=False):
    tower_size = 0
    for rock_number in range(rock_quantity): 
        if jet_index == 0 and search_cycle:
            print(f'Rock N°{rock_number} Rock index N°{rock_index}, Jet index N°{jet_index} and tower_size {tower_size}')
        
        rock = next_rock((SPAWN_X, tower_size + SPAWN_Y))
        rock_falling = True
        
        while rock_falling:
            jet = next_jet()
            pushed_rock = moved_rock(rock, jet)
            if is_position_legal(pushed_rock):
                rock = pushed_rock
            
            fallen_rock = moved_rock(rock, gravity)
            if is_position_legal(fallen_rock):
                rock = fallen_rock
            else:
                rock_falling = False
                for x, y in rock:
                    solid_rocks.add((x, y))
                    tower_size = max(tower_size, y)
    return tower_size

# Part 1
print(fall_rocks(rock_quantity = 2022))

# Part 2
rock_number = 1000000000000
print(fall_rocks(rock_quantity = 10000, search_cycle=True)) # Using the results to get the following numbers
rock_cycle_start, rock_cycle_length, towersize_cycle_length = 178, 60, 318
cycle_number = (rock_number - rock_cycle_start) // rock_cycle_length
remainder = (rock_number - rock_cycle_start) % rock_cycle_length
print(fall_rocks(rock_quantity = rock_cycle_start + remainder) + cycle_number * towersize_cycle_length)
