from re import findall

with open('day05.input', 'r') as drawing:
    half1, half2 = drawing.read().split('\n\n')
    layers = [[layer[1+i] for i in range(0, len(layer), 4)] for layer in half1.splitlines()[-2::-1]]
    procedure = (map(int, findall(r'\d+',step)) for step in half2.splitlines())

crate_stacks = [''.join(crate).strip() for crate in zip(*layers)]

def run_CrateMover(version, stacks):
    for quantity, origin, destination in procedure:
        origin, destination = origin - 1, destination - 1 # Data starts at 1, python at 0
        if version == 9000 :
            stacks[destination] += stacks[origin][:-quantity-1:-1] # Adding reversed quantity
        if version == 9001 :
            stacks[destination] += stacks[origin][-quantity:] # Adding quantity at once
        stacks[origin] = stacks[origin][:-quantity]
    return stacks

# Part 1
print(''.join([stack[-1] for stack in run_CrateMover(9000, crate_stacks)]))

# Part 2
print(''.join([stack[-1] for stack in run_CrateMover(9001, crate_stacks)]))
