with open('day02.input', 'r') as strategy_guide:
    rounds = [tuple(round.split()) for round in strategy_guide.read().splitlines()]

# Round: Opponent, Me
# Outcome score: Win/Draw/Lose = 6/3/0
# Round score = shape score + outcome of the round
# A: Rock (Shape score: 1)
# B: Paper (Shape score: 2)
# C: Scissors (Shape score: 3)
score = {'A': 1, 'B': 2, 'C': 3, 'X': 1, 'Y': 2, 'Z': 3}

# Part 1
# X: Rock
# Y: Paper
# Z: Scissors
print(rounds.count(('A', 'X')) * 4 # Rock vs Rock, Draw, 1 + 3
+ rounds.count(('A', 'Y')) * 8 # Rock vs Paper, Win, 2 + 6
+ rounds.count(('A', 'Z')) * 3 # Rock vs Scissors, Lose, 3 + 0
+ rounds.count(('B', 'X')) * 1 # Paper vs Rock, Lose, 1 + 0
+ rounds.count(('B', 'Y')) * 5 # Paper vs Paper, Draw, 2 + 3
+ rounds.count(('B', 'Z')) * 9 # Paper vs Scissors, Win, 3 + 6
+ rounds.count(('C', 'X')) * 7 # Scissors vs Rock, Win, 1 + 6
+ rounds.count(('C', 'Y')) * 2 # Scissors vs Paper, Lose, 2 + 0
+ rounds.count(('C', 'Z')) * 6) # Scissors vs Scissors, Draw, 3 + 3
# Can be simplified with this formula : x, y -> y + 3 * (1 - x + y % 3)
print(sum([score[my_shape] + 3 * ((1 - score[opponent_shape] + score[my_shape]) % 3) for opponent_shape, my_shape in rounds]))

# Part 2
# X: Lose
# Y: Draw
# Z: Win
print(rounds.count(('A', 'X')) * 3 # Lose against Rock: Scissors, 0 + 3
+ rounds.count(('A', 'Y')) * 4 # Draw against Rock: Rock, 3 + 1
+ rounds.count(('A', 'Z')) * 8 # Win against Rock: Paper, 6 + 2
+ rounds.count(('B', 'X')) * 1 # Lose against Paper: Rock, 0 + 1
+ rounds.count(('B', 'Y')) * 5 # Draw against Paper: Paper, 3 + 2
+ rounds.count(('B', 'Z')) * 9 # Win against Paper: Scissors, 6 + 3
+ rounds.count(('C', 'X')) * 2 # Lose against Scissors: Paper, 0 + 2
+ rounds.count(('C', 'Y')) * 6 # Draw against Scissors: Scissors, 3 + 3
+ rounds.count(('C', 'Z')) * 7) # Win against Scissors: Rock, 6 + 1
# Can be simplified with this formula : x, y -> 3 * (y - 1) + (x + y) % 3 + 1
print(sum([3 * (score[outcome] - 1) + ((score[opponent_shape] + score[outcome]) % 3) + 1 for opponent_shape, outcome in rounds]))