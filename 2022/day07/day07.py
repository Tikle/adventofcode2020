with open('day07.input', 'r') as history_file:
    history = [[line.split() for line in commands.splitlines()] for commands in history_file.read().split('$ ')[2:] ]
    # The file tree is perfectly parsed depth first

size_pile = [0]
directory_sizes = []
for command_block in history:
    if command_block[0][0] == 'ls':
        size_pile = [dir_size + sum([int(size) for size, _ in command_block[1:] if size != 'dir']) for dir_size in size_pile ] # Adding every file size to each directory of the path
                
    if command_block[0][0] == 'cd':
        if command_block[0][1] == '..':
            directory_sizes.append(size_pile.pop())
        else:
            size_pile.append(0)
directory_sizes.extend(size_pile)
            
# Part 1
print(sum([size for size in directory_sizes if size < 100000]))

# Part 2
minimum_deletion_size = 30000000 - (70000000 - size_pile[0])
directory_sizes.sort()
print([size for size in directory_sizes if size > minimum_deletion_size][0])