with open('day14.input', 'r') as trace:
    paths = [[tuple(map(int, point.split(','))) for point in line.split(' -> ') ] for line in trace.read().splitlines() ]

sand = set()
rocks = set()
for path in paths:
    for index in range(len(path) - 1):
        end1, end2 = path[index], path[index + 1]
        if end1[0] == end2[0]: # Vertical segment
            for y in range(min(end1[1], end2[1]), max(end1[1], end2[1]) + 1):
                rocks.add((end1[0], y))
        else :  # Horizontal segment
            for x in range(min(end1[0], end2[0]), max(end1[0], end2[0]) + 1):
                rocks.add((x, end1[1]))

source_tile = (500, 0)
lowest_rock_y = max(y for _, y in rocks)
floor_y = lowest_rock_y + 2
            
def next_tile(tile, floor=False):
    x, y = tile
    
    if not floor and y > lowest_rock_y:
        raise Exception("End part 1")
        
    if y + 1 != floor_y:
        for next in [(x, y + 1), (x - 1, y + 1), (x + 1, y + 1)]:
            if next not in rocks and next not in sand:
                next_tile(next, floor)
    sand.add(tile)
    
    if floor and tile == source_tile:
        raise Exception("End part 2")
    
# Part 1
try:
    while 1: 
        next_tile(source_tile)
except Exception as e:
    print(len(sand))
 
# Part 2
try:
    while 1: 
        next_tile(source_tile, floor=True)
except Exception as e:
    print(len(sand))
    
    
