from re import findall
from itertools import permutations
      
with open('day16.input', 'r') as scan_output:
    valve_data = [findall(r'[A-Z]{2}|\d+', line) for line in scan_output.read().splitlines()]
    valves = [valve[0] for valve in valve_data]
    flow_rate = {valve[0]: int(valve[1]) for valve in valve_data}
    next_valves = {valve[0]: valve[2:] for valve in valve_data}

def shortest_path_between(source_valve, destination_valve):
    valves_visited = set()
    paths_queue = [[source_valve]]
    while paths_queue:
        path_to_valve = paths_queue.pop(0)
        valve = path_to_valve[-1]
        if valve == destination_valve:
            return path_to_valve
        for next_valve in next_valves[valve]:
            if next_valve not in valves_visited:
                valves_visited.add(next_valve)
                path_to_next_valve = path_to_valve + [next_valve]
                paths_queue.append(path_to_next_valve)
    else:
        return []
     
useful_valves = [valve for valve in valves if flow_rate[valve] != 0 or valve == 'AA']
time_to_valve = {tunnel: len(shortest_path_between(*tunnel)) - 1 for tunnel in list(permutations(useful_valves, 2))}

def max_pressure_path(time_left, starting_path, elephant=False):
    max_pressure = 0
    queue = [(0, time_left, starting_path)]
    while queue:
        pressure, minutes, path = queue.pop(0)
        if pressure > max_pressure:
            max_pressure = pressure
        remaining_valves = []
        for valve in useful_valves:
            if valve not in path:
                if minutes - time_to_valve[(path[-1], valve)] - 1 >= 0:
                    remaining_valves.append(valve)
        if remaining_valves:
            for next_valve in remaining_valves:
                next_minutes = minutes - time_to_valve[(path[-1], next_valve)] - 1 # Time spent following best path + opening the valve
                next_path = path + [next_valve]
                next_pressure = pressure + next_minutes * flow_rate[next_valve]
                queue.append((next_pressure, next_minutes, next_path))     
        else:
            if elephant:
                elephant_pressure = max_pressure_path(time_left, path)
                if elephant_pressure + pressure > max_pressure:
                    max_pressure = elephant_pressure + pressure
    return max_pressure

# # Part 1 # 1651, 1850
print(max_pressure_path(30, ['AA']))

# Part 2 # 1707, 2306
print(max_pressure_path(26, ['AA'], elephant=True))

