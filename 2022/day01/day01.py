with open('day01.input','r') as input_file:
    inventories = [map(int, inventory.splitlines()) for inventory in input_file.read().split('\n\n')]

calories_totals = [sum(calories) for calories in inventories]

# Part One
print(max(calories_totals))

# Part two
print(sum(sorted(calories_totals)[-3:]))
