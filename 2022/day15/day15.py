from re import findall

with open('day15.input', 'r') as sensors_report:
    data = [[tuple(int(number) for number in findall(r'-?\d+', text)) for text in line.split(':')] for line in sensors_report.read().splitlines() ]

def manhattan_distance(A, B):
    xa, ya = A
    xb, yb = B
    return abs(xa - xb) + abs(ya - yb)

sensors = {sensor: manhattan_distance(sensor, beacon) for sensor, beacon in data}
BOUNDARY = 4000000

# Part 1
y = 2000000
not_x = set()
for sensor, beacon in data:
    sx, sy = sensor
    bx, by = beacon
    scope_x = manhattan_distance(sensor, beacon) - abs(sy - y)
    for x in range(sx - scope_x, sx + scope_x + 1):
        if not (y == by and x == bx):
            not_x.add(x) 
print(len(not_x))

def tuning_frequency(B):
    x, y = B
    return BOUNDARY * x + y
                
def points_on_circle(center, radius):
    cx, cy = center
    for x in range(max(0, cx - radius), min(BOUNDARY, cx + radius) + 1):
        height = radius - abs(x - cx)
        for y in [cy + height, cy - height]:
            if y in range(BOUNDARY + 1):
                yield (x, y) 

def is_away_from_sensors(point):
    for sensor, radius in sensors.items():
        if manhattan_distance(sensor, point) <= radius:
            return False
    return True

# Part 2
for sensor, radius in sensors.items():
    for point in points_on_circle(sensor, radius + 1):
        if is_away_from_sensors(point):
            print(tuning_frequency(point))
            exit(0)


                

