with open('day13.input', 'r') as distress_signal:
    lines = [eval(line) for line in distress_signal.read().splitlines() if line != '']
    
def parse_pair(left_side, right_side):
    k = 0
    while True:
        if len(left_side) == k == len(right_side):
            return None
        elif k == len(right_side):
            return False
        elif k == len(left_side):
            return True
        
        L, R = left_side[k], right_side[k]
        
        if isinstance(L, int) and isinstance(R, int):
            if L < R:
                return True
            elif L > R:
                return False
        else:
            if isinstance(L, list) and isinstance(R, int):
                result = parse_pair(L, [R])
            if isinstance(L, int) and isinstance(R, list):
                result = parse_pair([L], R)
            if isinstance(L, list) and isinstance(R, list):
                result = parse_pair(L, R)
            if result != None: 
                return result
        k += 1

# Part 1
print(sum([index // 2 + 1 for index in range(0, len(lines), 2) if parse_pair(lines[index], lines[index + 1])]))

# Part 2
number_lines_inferior_to_2=sum([parse_pair(lines[index], [[2]]) for index in range(len(lines))])
number_lines_inferior_to_6=sum([parse_pair(lines[index], [[6]]) for index in range(len(lines))]) + 1
print((number_lines_inferior_to_2 + 1) * (number_lines_inferior_to_6 + 1))
